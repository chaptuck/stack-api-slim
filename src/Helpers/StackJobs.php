<?php
namespace Helpers;

use Models\City;

class StackJobs {

    protected $xml;
    protected $params;
    protected $features;
    protected $jobCount;
    protected $numberPages;
    protected $page = 0;
    protected $lastPage;
    protected $perPage = 250;
    protected $pageCount;

    public function __construct($xml, $params) {
        $this->setFeatures($xml)
            ->setJobCount()
            ->setPageValues($params);
    }

    private function setFeatures($xml)
    {
      $xml_parse = simplexml_load_string($xml);
      $items = $xml_parse->xpath("/rss/channel/item");

      $features = Array();
      foreach ($items as $item) {
          $categories = Array();
          foreach ($item->category as $cat) {
              array_push($categories, (string) $cat);
          }
          $parsedTitle = self::parseTitle($item->title);
          $feature = Array(
              "type" => "Feature",
              "geometry" => (object) Array(
                  "type" => "Point",
                  "coordinates" => self::getCityCoordinates((string) $item->location)
              ),
              "properties" => Array(
                  "guid" => (int) $item->guid,
                  "link" => (string) $item->link,
                  "title" => (string) $parsedTitle["title"],
                  "company" => (string) $parsedTitle["company"],
                  "remote" => (string) $parsedTitle["remote"],
                  //"description" => (string) $item->description,
                  "pubDate" => (string) $item->pubDate,
                  "location" => (string) $item->location,
                  "category" => $categories
              ),
          );
          array_push($features, $feature);
      }

      $this->features = $features;

      return $this;
    }

    public function getGeoJsonArray()
    {
        $data = Array(
            "type" => "FeatureCollection",
            "features" => $this->getFeatures()
        );

        return $data;
    }

    private function setJobCount()
    {
        $this->jobCount = count($this->features);
        return $this;
    }

    /**
    * Parse the title returned from Stack Overflow
    * Job Title at Company (City, ST) (allows remote)
    * @param String $title
    * @return AssociativeArray
    */
    private function parseTitle($title) {
        $explodeTitle = explode(" at ", $title);
        $title = $explodeTitle[0];
        $explodeCompany = explode(" (", $explodeTitle[1]);
        $company = $explodeCompany[0];
        $remote = "";
        if (count($explodeCompany) > 2) {
            $remote = "(allows remote)";
        }
        return Array(
            "title" => $title,
            "company" => $company,
            "remote" => $remote
        );
    }

    private function getCityCoordinates($city)
    {
        $cityQuery = new City();
        $cityName = "remote";
        $stateName = "us";
        if ($city) {
            $pieces = explode(",", $city);
            if (count($pieces) > 1) {
                $cityName = trim(strtolower($pieces[0]));
                $stateName = trim(strtolower($pieces[1]));
            }
        }
        $coordinates = $cityQuery->getCoordinates($cityName, $stateName);
        return $coordinates;
    }

    private function setPageValues($params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case "page":
                    $this->page = (int) $value;
                    break;
                case "perPage":
                    $this->perPage = (int) $value;
                    break;
            }
        }

        $this->lastPage = floor($this->jobCount / $this->perPage) - 1; # pages are zero based

        return $this;
    }

    private function setPagecount($count)
    {
        $this->pageCount = $count;
        return $this;
    }

    public function getFeatures() {
        $page = $this->getPage();
        $perPage = $this->getPerPage();
        if ($this->getJobCount() <= $perPage) {
            $this->setPageCount($this->jobCount);
            return $this->features;
        }

        $startPage = $page * $perPage;
        $data = array_slice($this->features, $startPage, $perPage);

        $this->setPageCount(count($data));
        return $data;
    }

    public function getJobCount()
    {
        return $this->jobCount;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getPerPage()
    {
        return $this->perPage;
    }

    public function getPageCount()
    {
        return $this->pageCount;
    }

    public function getLastPage()
    {
        return $this->lastPage;
    }
}
