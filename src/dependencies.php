<?php
// DIC configuration

$container = $app->getContainer();

$container['db'] = function($c) {
    $settings = $c->get('settings')['db'];
    $db = \RedBeanPHP\R::setup(
        "mysql:host=" .$settings['host']. ";dbname=".$settings['database'],
        $settings['username'],
        $settings['password']
     );
    \RedBeanPHP\R::freeze('true');

    return $db;
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['Models\IndexController'] = function ($c) {
    return new Models\IndexController($c['router'], $c['logger'], $c['db']);
};
