<?php
namespace Models;
use \RedBeanPHP\R as R;

class City extends \RedBeanPHP\SimpleModel {

    public function __construct() {
    }

    public function getAll()
    {
        $cities = R::getAll( 'SELECT * FROM city;' );
        return $cities;
    }

    public function getCoordinates($city, $state)
    {
        $city = R::findOne("city", "name = ? and state = ?", [$city, $state]);
        if (!$city) {
            $city = R::findOne("city", "name = ? and state = ?", ["not found", "us"]);
        }
        $properties = $city->getProperties();
        $latitude = (float) $properties['latitude'];
        $longitude = (float) $properties['longitude'];
        $coordinates = [$longitude, $latitude];
        return $coordinates;
    }
}
