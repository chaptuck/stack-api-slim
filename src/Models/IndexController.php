<?php

namespace Models;

use Slim\Router;
use \GuzzleHttp\Client;
use Models\City;
use Monolog\Logger as Logger;
use Helpers\StackJobs as StackJobs;

class IndexController {
    private $router;

    public function __construct(Router $router, Logger $logger)
    {
        $this->router = $router;
        $this->logger = $logger;
    }

    public function stackJobs($request, $response, $params)
    {
        $requestParams = $request->getParsedBody();
        $stackURI = self::formatStackRequest($requestParams);
        $client = new Client();
        $res = $client->request('GET', $stackURI);
        $code = $res->getStatusCode();
        $xml = $res->getBody()->getContents();
        $stackJobs = new StackJobs($xml, $requestParams, $this->logger);
        $geojsonData = $stackJobs->getGeoJsonArray();
        $data = array(
            'time'=> time(),
            'job_count' => $stackJobs->getJobCount(),
            'page' => $stackJobs->getPage(),
            'lastPage' => $stackJobs->getLastPage(),
            'perPage' => $stackJobs->getPerPage(),
            'pageCount' => $stackJobs->getPageCount(),
            'data' => $geojsonData,
        );
        $jsonResponse = $response->withJson($data, 201);
        return $jsonResponse;
    }

    private function formatStackRequest($params)
    {
        $uri = "https://stackoverflow.com/jobs/feed?";
        if (!is_array($params)) { return $uri; }
        foreach ($params as $key => $value) {
            switch ($key) {
                case "location":
                    $uri .= "l=" . $value . "&";
                    break;
                case "search":
                    $uri .= "q=" . $value . "&";
                    break;
                case "distance":
                    $uri .= "d=" . $value . "&";
                    break;
                case "compensation":
                    $uri .= "s=" . $value . "&";
                    break;
                case "currency":
                    $uri .= "c=" . $value . "&";
                    break;
                case "remote":
                    $uri .= "r=" . $value . "&";
                    break;
                case "type":
                    $uri .= "j=" . $value . "&";
                    break;
                case "visa":
                    $uri .= "v=" . $value ."&";
                    break;
            }
        }
        return $uri;
    }
}
